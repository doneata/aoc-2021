import math
import pdb
import re
import sys

from collections import Counter, defaultdict, namedtuple
from datetime import datetime
from itertools import (
    chain,
    count,
    cycle,
    dropwhile,
    groupby,
    permutations,
    product,
    repeat,
    takewhile,
)
from functools import lru_cache, partial, reduce
from typing import Dict, Tuple, Set, List, Iterable, Iterator, Optional, Union

from toolz import (
    concat,
    concatv,
    first,
    identity,
    iterate,
    nth,
    partition,
    second,
    sliding_window,
    take,
)

import numpy as np


try:
    current_day = int(sys.argv[1])
except:
    current_day = datetime.today().day


def load_data(day: int, parser=str, sep="\n") -> list:
    "Split the day's input file into sections separated by `sep`, and apply `parser` to each."
    sections = open(f"data/{day:02d}.txt").read().rstrip().split(sep)
    return [parser(section) for section in sections]


def quantify(iterable, pred=bool) -> int:
    "Count the number of items in iterable for which pred is true."
    return sum(1 for item in iterable if pred(item))


def multimap(items: Iterable[Tuple]) -> dict:
    "Given (key, val) pairs, return {key: [val, ....], ...}."
    result = defaultdict(list)
    for (key, val) in items:
        result[key].append(val)
    return result


def clamp(x, α, ω):
    return max(α, min(x, ω))


cat = "".join


def total(counter: Counter) -> int:
    """The sum of all the counts in a Counter."""
    return sum(counter.values())


def ints(text: str) -> Tuple[int]:
    """A tuple of all the integers in text, ignoring non-number characters."""
    return tuple(map(int, re.findall(r"-?[0-9]+", text)))


def day01a(nums: List[int]):
    def lower_than(xy):
        x, y = xy
        return x < y

    return quantify(zip(nums, nums[1:]), lower_than)


def day01b(nums: List[int]):
    return day01a(list(map(sum, sliding_window(3, nums))))


def day02a(commands: List[tuple]):
    Pos = namedtuple("Pos", "x y")

    MOVES = {
        "forward": lambda pos, step: Pos(pos.x + step, pos.y),
        "up": lambda pos, step: Pos(pos.x, pos.y - step),
        "down": lambda pos, step: Pos(pos.x, pos.y + step),
    }

    def f(pos, command):
        direction, step = command
        return MOVES[direction](pos, step)

    pos = reduce(f, commands, Pos(0, 0))
    return pos.x * pos.y


def day02b(commands: List[tuple]):
    Pos = namedtuple("Pos", "x y aim")

    MOVES = {
        "forward": lambda pos, step: Pos(pos.x + step, pos.y + pos.aim * step, pos.aim),
        "up": lambda pos, step: Pos(pos.x, pos.y, pos.aim - step),
        "down": lambda pos, step: Pos(pos.x, pos.y, pos.aim + step),
    }

    def f(pos, command):
        direction, step = command
        return MOVES[direction](pos, step)

    pos = reduce(f, commands, Pos(0, 0, 0))
    return pos.x * pos.y


def bin_to_int(vals: tuple):
    return int("".join(map(str, vals)), 2)


def day03a(xss: List[tuple]):
    def f(accs, vals):
        return tuple(a + v for a, v in zip(accs, vals))

    n = len(xss)
    m = len(xss[0])
    counts = reduce(f, xss, tuple(0 for _ in range(m)))
    gamma = [int(c > n // 2) for c in counts]
    epsilon = [int(not bool(c)) for c in gamma]
    return bin_to_int(gamma) * bin_to_int(epsilon)


def day03b(xss: List[tuple]):
    n = len(xss)
    m = len(xss[0])

    def f(i, acc, vals):
        return acc + vals[i]

    def go(get_b):
        yss = xss.copy()
        for i in range(m):
            c = reduce(partial(f, i), yss, 0)
            b = get_b(c, len(yss))
            yss = [ys for ys in yss if ys[i] == b]
            if len(yss) == 1:
                return bin_to_int(yss[0])

    most_common = lambda c, n: 1 if c >= n / 2 else 0
    least_common = lambda c, n: 0 if c >= n / 2 else 1
    return go(most_common) * go(least_common)


def parse_board(lines):
    return [[int(elem) for elem in line.split()] for line in lines if line]


def is_bingo(board, numbers):
    def transpose(board):
        return zip(*board)

    def is_bingo_rows(board):
        return any(all(elem in numbers for elem in row) for row in board)

    def is_bingo_cols(board):
        return is_bingo_rows(transpose(board))

    return is_bingo_rows(board) or is_bingo_cols(board)


def score_board(board, numbers):
    unmarked = [elem for elem in concat(board) if elem not in numbers]
    return sum(unmarked) * numbers[-1]


def day04a(numbers, boards):
    for i in range(len(numbers)):
        selected = numbers[: i + 1]
        winners = [board for board in boards if is_bingo(board, selected)]
        try:
            winner = first(winners)
            return score_board(winner, selected)
        except StopIteration:
            pass


def day04b(numbers, boards):
    for i in range(len(numbers)):
        selected = numbers[: i + 1]
        if len(boards) == 1 and is_bingo(boards[0], selected):
            return score_board(boards[0], selected)
        boards = [board for board in boards if not is_bingo(board, selected)]


def day05a(data):
    # Better approach:
    # > length . filter (> 1) . freqs . concatMap lineTo . sameLine
    points = defaultdict(int)
    for ((x1, y1), (x2, y2)) in data:
        if x1 == x2:
            y_min = min(y1, y2)
            y_max = max(y1, y2)
            for y in range(y_min, y_max + 1):
                points[(x1, y)] += 1
        if y1 == y2:
            x_min = min(x1, x2)
            x_max = max(x1, x2)
            for x in range(x_min, x_max + 1):
                points[(x, y1)] += 1
    return quantify(points.values(), lambda v: v >= 2)


def day05b(data):
    points = defaultdict(int)

    def sign(e):
        if e > 0:
            return 1
        elif e < 0:
            return -1
        else:
            return 0

    for ((x1, y1), (x2, y2)) in data:
        if x1 == x2:
            y_min = min(y1, y2)
            y_max = max(y1, y2)
            for y in range(y_min, y_max + 1):
                points[(x1, y)] += 1
        if y1 == y2:
            x_min = min(x1, x2)
            x_max = max(x1, x2)
            for x in range(x_min, x_max + 1):
                points[(x, y1)] += 1
        if abs(x2 - x1) == abs(y2 - y1):
            x = x1
            y = y1
            dx = sign(x2 - x1)
            dy = sign(y2 - y1)
            while x != x2 or y != y2:
                points[(x, y)] += 1
                x += dx
                y += dy
            points[(x, y)] += 1

    # for i in range(10):
    #     for j in range(10):
    #         v = str(points[(j, i)] or ".")
    #         print(v, end=" ")
    #     print()
    return quantify(points.values(), lambda v: v >= 2)


class Fish:
    def __init__(self, start=8):
        self.state = start
        self.to_emit = False

    def __next__(self):
        self.state -= 1
        if self.state < 0:
            self.state = 6
            self.to_emit = True
        else:
            self.to_emit = False
        return self.state

    def __str__(self):
        return str(self.state)

    def __repr__(self):
        return "Fish({})".format(self.state)


def day06a(data):
    fish = [Fish(val) for val in data]
    for day in range(80):
        for f in fish:
            _ = next(f)
        fish = fish + [Fish() for f in fish if f.to_emit]
    return len(fish)


def day06b(data):
    @lru_cache
    def go(val, days):
        if days == 0:
            return 1 if val >= 0 else 2
        if val == 0:
            return go(6, days - 1) + go(8, days - 1)
        else:
            return go(val - 1, days - 1)

    return sum(go(val, 256) for val in data)


def day07a(numbers):
    numbers = np.array(numbers)
    val = np.median(numbers)
    return np.sum(np.abs(numbers - val))


def day07b(numbers):
    def cost(x, n):
        d = abs(x - n)
        return d * (d + 1) / 2

    def obj(x):
        return sum(cost(x, n) for n in numbers)

    α, ω = min(numbers), max(numbers)
    f, _ = min((obj(val), val) for val in range(α, ω + 1))
    return f


def day08a(data):
    digit_to_segments = {
        0: "abcefg",
        1: "cf",
        2: "acdeg",
        3: "acdfg",
        4: "bcdf",
        5: "abdfg",
        6: "abdefg",
        7: "acf",
        8: "abcdefg",
        9: "abcdfg",
    }
    digit_to_length = {d: len(s) for d, s in digit_to_segments.items()}
    length_to_digits = multimap([(l, d) for d, l in digit_to_length.items()])
    selected_lengths = set(k for k, v in length_to_digits.items() if len(v) == 1)
    outputs = concat(output for _, output in data)
    return quantify(outputs, lambda o: len(o) in selected_lengths)


def day08b(data):
    # TODO
    # - [ ] str.maketrans
    # - [ ] cat = "".join
    digit_to_segments = {
        0: "abcefg",
        1: "cf",
        2: "acdeg",
        3: "acdfg",
        4: "bcdf",
        5: "abdfg",
        6: "abdefg",
        7: "acf",
        8: "abcdefg",
        9: "abcdfg",
    }
    segment_to_digit = {s: d for d, s in digit_to_segments.items()}
    segments = set(digit_to_segments.values())
    c = "abcdefg"

    def get_perm_dict(perm):
        return {c[i]: c[j] for i, j in enumerate(perm)}

    def permute(word, perm_dict):
        return "".join(sorted(perm_dict[c] for c in word))

    def do1(signals, output):
        numbers = signals + output
        perm = first(
            p
            for p in permutations(range(7), 7)
            if all(permute(n, get_perm_dict(p)) in segments for n in numbers)
        )
        perm_dict = get_perm_dict(perm)
        return int(
            "".join(str(segment_to_digit[permute(o, perm_dict)]) for o in output)
        )

    return sum(do1(s, o) for s, o in data)


def sliding_window_2d(data):
    num_rows = len(data)
    num_cols = len(data[0])
    for r in range(num_rows):
        for c in range(num_cols):
            res = {"center": data[r][c]}
            # "r": r, "c": c,
            if 0 <= r - 1:
                res["top"] = data[r - 1][c]
            if r + 1 < num_rows:
                res["bottom"] = data[r + 1][c]
            if 0 <= c - 1:
                res["left"] = data[r][c - 1]
            if c + 1 < num_cols:
                res["right"] = data[r][c + 1]
            yield res


def day09a(data):
    def is_low_point(window):
        return all(
            window["center"] < neighbour
            for name, neighbour in window.items()
            if name != "center"
        )

    return sum(
        window["center"] + 1
        for window in sliding_window_2d(data)
        if is_low_point(window)
    )


def day09b(data):
    # def is_high_point(window):
    #     return all(
    #         window["center"] > neighbour
    #         for name, neighbour in window.items()
    #         if name in {"top", "right", "left", "bottom"}
    #     )

    # num_rows = len(data)
    # num_cols = len(data[0])

    # for window in sliding_window_2d(data):
    #     if is_high_point(window):
    #         # print(window)
    #         if window["center"] != 9:
    #             r, c = window["r"], window["c"]
    #             r1 = clamp(r - 2, 0, num_rows - 1)
    #             r2 = clamp(r + 3, 0, num_rows - 1)
    #             c1 = clamp(c - 2, 0, num_cols - 1)
    #             c2 = clamp(c + 3, 0, num_cols - 1)
    #             print(np.array(data)[r1: r2, c1: c2])
    #             pdb.set_trace()
    # TODO Alternative, implement using flood fill?

    from skimage.measure import label, regionprops

    image = (np.array(data) < 9).astype(int)
    labels = label(image, connectivity=1)
    areas = sorted([region.area for region in regionprops(labels)])
    return np.prod(areas[-3:])


pairs = ["<>", "()", "[]", "{}"]


def is_open(x):
    return any(x == s for s, _ in pairs)


def matches(x, y):
    return any(x == s and y == e for s, e in pairs)


def parse_syntax(line):
    stack = []
    for c in line:
        if is_open(c):
            stack.append(c)
        else:
            k = stack.pop()
            if not matches(k, c):
                return {
                    "type": "failed",
                    "found": c,
                }
    return {
        "type": "finished",
        "stack": stack,
    }


def day10a(data):
    score = {
        ")": 3,
        "]": 57,
        "}": 1197,
        ">": 25137,
    }
    results = map(parse_syntax, data)
    return sum(score[r["found"]] for r in results if r["type"] == "failed")


def day10b(data):
    score = {
        "(": 1,
        "[": 2,
        "{": 3,
        "<": 4,
    }

    def get_score(stack):
        # TODO this function is computing the score base 5!
        s = 0
        while stack:
            c = stack.pop()
            s = s * 5 + score[c]
        return s

    return int(
        np.median(
            [
                get_score(r["stack"])
                for r in map(parse_syntax, data)
                if r["type"] == "finished" and r["stack"]
            ]
        )
    )


def day11a(grid):
    num_flashes = 0
    inc = lambda x: x + 1
    zero = lambda _: 0

    num_rows = len(grid)
    num_cols = len(grid[0])

    def traverse(grid):
        for r, row in enumerate(grid):
            for c, elem in enumerate(row):
                yield (r, c), elem

    def update(grid, coords, f):
        return [
            [f(elem) if (r, c) in coords else elem for c, elem in enumerate(row)]
            for r, row in enumerate(grid)
        ]

    def map_grid(grid, f):
        return [[f(elem) for elem in row] for row in grid]

    def print_grid(grid):
        for row in grid:
            for elem in row:
                print(elem, end=" ")
            print()
        print()

    def neighbours(r, c):
        coords = [
            (r - 1, c - 1),
            (r - 1, c),
            (r - 1, c + 1),
            (r, c - 1),
            (r, c + 1),
            (r + 1, c - 1),
            (r + 1, c),
            (r + 1, c + 1),
        ]
        return [(x, y) for x, y in coords if 0 <= x < num_rows and 0 <= y < num_cols]

    for step in range(100):
        grid = map_grid(grid, inc)
        flash_all = []
        while True:
            flash_cur = [coord for coord, elem in traverse(grid) if elem > 9]
            flash_all.extend(flash_cur)
            num_flashes += len(flash_cur)
            if not flash_cur:
                break
            else:
                for coord in flash_cur:
                    grid = update(grid, neighbours(*coord), inc)
            grid = update(grid, flash_all, zero)
        # print_grid(grid)

    return num_flashes


def day11b(grid):
    num_flashes = 0
    inc = lambda x: x + 1
    zero = lambda _: 0

    num_rows = len(grid)
    num_cols = len(grid[0])

    def traverse(grid):
        for r, row in enumerate(grid):
            for c, elem in enumerate(row):
                yield (r, c), elem

    def update(grid, coords, f):
        return [
            [f(elem) if (r, c) in coords else elem for c, elem in enumerate(row)]
            for r, row in enumerate(grid)
        ]

    def map_grid(grid, f):
        return [[f(elem) for elem in row] for row in grid]

    def print_grid(grid):
        for row in grid:
            for elem in row:
                print(elem, end=" ")
            print()
        print()

    def neighbours(r, c):
        coords = [
            (r - 1, c - 1),
            (r - 1, c),
            (r - 1, c + 1),
            (r, c - 1),
            (r, c + 1),
            (r + 1, c - 1),
            (r + 1, c),
            (r + 1, c + 1),
        ]
        return [(x, y) for x, y in coords if 0 <= x < num_rows and 0 <= y < num_cols]

    for step in count(1):
        grid = map_grid(grid, inc)
        flash_all = []
        while True:
            flash_cur = [coord for coord, elem in traverse(grid) if elem > 9]
            flash_all.extend(flash_cur)
            num_flashes += len(flash_cur)
            if not flash_cur:
                break
            else:
                for coord in flash_cur:
                    grid = update(grid, neighbours(*coord), inc)
            grid = update(grid, flash_all, zero)
            # print_grid(grid)
            if len(flash_all) == num_rows * num_cols:
                return step


def day12a(edges):
    # Norvig used recursion and "yield from"
    def is_done(path):
        return path[-1] == "end"

    def get_next(path):
        curr = path[-1]
        for node in edges[curr]:
            if node.islower() and node in path:
                continue
            yield node

    paths = [["start"]]
    while True:
        if all(is_done(path) for path in paths):
            break
        paths_new = []
        for path in paths:
            if is_done(path):
                paths_new.append(path)
            else:
                for node in get_next(path):
                    paths_new.append(path + [node])
        paths = paths_new

    # for path in paths:
    #     print(" ".join(path))
    return len(paths)


def day12b(edges):
    def is_done(path):
        return path[-1] == "end"

    def get_next(path):
        curr = path[-1]
        for node in edges[curr]:
            yield node

    def is_valid(path):
        counts = Counter(path)
        counts_lower = [v for k, v in counts.items() if k.islower()]
        counts_counts_lower = Counter(counts_lower)
        return (
            counts["start"] == 1
            and counts["end"] <= 1
            and max(counts_counts_lower.keys()) <= 2
            and counts_counts_lower[2] <= 1
        )

    paths = [["start"]]
    while True:
        if all(is_done(path) for path in paths):
            break
        paths_new = []
        for path in paths:
            if is_done(path):
                paths_new.append(path)
            else:
                paths_new_ = [p for n in get_next(path) if is_valid(p := path + [n])]
                paths_new.extend(paths_new_)
        paths = paths_new

    return len(paths)


def day13(points, folds, part):
    def print_points(points):
        print(len(points))
        for row in range(max(p[1] for p in points) + 1):
            for col in range(max(p[0] for p in points) + 1):
                print("■" if (col, row) in points else " ", end="")
            print()

    def fold_x(points, v):
        return {(x, y) for x, y in points if x < v} | {
            (2 * v - x, y) for x, y in points if x > v
        }

    def fold_y(points, v):
        return {(x, y) for x, y in points if y < v} | {
            (x, 2 * v - y) for x, y in points if y > v
        }

    FOLD = {
        "x": fold_x,
        "y": fold_y,
    }
    # print_points(points)
    for d, v in folds:
        points = FOLD[d](points, v)
        if part == "a":
            return len(points)

    print_points(points)


def day14(polymer, rules, num_steps, verbose=False):
    def dup(x):
        return [x, x]

    def total_second(xys) -> int:
        return sum(map(second, xys))

    def expand_counter(f, counter):
        """Apply f to the keys of the counter and re-count the new elements."""
        elems_counts = sorted(concat(zip(f(v), dup(c)) for v, c in counter.items()))
        return {k: total_second(g) for k, g in groupby(elems_counts, key=first)}

    def step(polymer):
        return expand_counter(lambda p: rules[p], polymer)

    def most_common(counter):
        return max(counter.items(), key=second)

    def least_common(counter):
        return min(counter.items(), key=second)

    def div2up(n):
        return int(math.ceil(n / 2))

    polymer = nth(num_steps, iterate(step, polymer))
    elem_counts = expand_counter(identity, polymer)

    _, m = most_common(elem_counts)
    _, n = least_common(elem_counts)

    if verbose:
        print(polymer)
        print(total(polymer) + 1)
        print(elem_counts)
        print(most_common(elem_counts))
        print(least_common(elem_counts))
        print()

    return div2up(m) - div2up(n)


def day15a(grid):
    # wrong solution
    # @lru_cache
    # def go(x, y):
    #     curr = grid[x][y]
    #     if x == 0 and y == 0:
    #         return 0
    #     elif x == 0:
    #         return curr + go(x, y - 1)
    #     elif y == 0:
    #         return curr + go(x - 1, y)
    #     else:
    #         return curr + min(go(x - 1, y), go(x, y - 1))

    # num_rows = len(grid)
    # num_cols = len(grid[0])
    # return go(num_rows - 1, num_cols - 1)

    cost = mapg(lambda _: 0, grid)
    for i, row in enumerate(grid):
        for j, elem in enumerate(row):
            if i == 0 and j == 0:
                cost[i][j] = 0
            elif i == 0:
                cost[i][j] = elem + cost[i][j - 1]
            elif j == 0:
                cost[i][j] = elem + cost[i - 1][j]
            else:
                cost[i][j] = elem + min(cost[i - 1][j], cost[i][j - 1])
    return cost[-1][-1]


def day15b(grid):
    import heapq
    from dataclasses import dataclass

    num_rows = len(grid)
    num_cols = len(grid[0])

    def mapg(f, grid):
        return [[f(elem) for elem in row] for row in grid]

    @dataclass
    class Node:
        row: int
        col: int

        @property
        def f(node):
            if not node.parent:
                return 0
            else:
                return node.parent.f + grid[node.row][node.col]

        def h(node):
            len_path = (num_rows - node.row - 1) + (num_cols - node.col - 1)
            # remaining = concatv(concat(grid[node.row + 1:]), grid[node.row][node.col + 1:])
            # return sum(sorted(remaining)[:len_path])
            return len_path

        @property
        def score(self):
            return self.f + self.h()

        def __lt__(self, other):
            return self.score < other.score

        def __str__(self):
            return "(row={}, col={}, score={})".format(self.row, self.col, self.score)

        def __repr__(self):
            return "({}, {}, {})".format(self.row, self.col, self.score)

        def path_str(self):
            return f"{self.row} {self.col}, " + (
                self.parent.path_str() if self.parent else ""
            )

    def neighbours(r, c):
        coords = [
            (r - 1, c),
            (r, c - 1),
            (r, c + 1),
            (r + 1, c),
        ]
        return [(x, y) for x, y in coords if 0 <= x < num_rows and 0 <= y < num_cols]

    def is_goal(node):
        return node.row == num_rows - 1 and node.col == num_cols - 1

    def step(state):
        links, vertices = state
        curr_cost, curr_vertex = min((links[v]["cost"], v) for v in vertices)
        vertices.remove(curr_vertex)
        for r, c in neighbours(*curr_vertex):
            if curr_cost + grid[r][c] < links[(r, c)]["cost"]:
                links[(r, c)] = {"parent": curr_vertex, "cost": curr_cost + grid[r][c]}
        return links, vertices

    vertices = {(r, c) for r in range(num_rows) for c in range(num_cols)}
    links = {
        (r, c): {
            "parent": (r, c),
            "cost": np.inf,
        }
        for r in range(num_rows)
        for c in range(num_cols)
    }
    start = (0, 0)
    end = (num_rows - 1, num_cols - 1)
    links[start] = {"parent": start, "cost": 0}

    state = links, vertices
    for it in range(len(vertices)):
        state = step(state)

    return state[0][end]

    # frontier = []
    # # origin = Node(0, 0, None)

    # cost = mapg(lambda _: np.inf, grid)

    # start = (0, 0)

    # frontier = []
    # heapq.heapify(frontier)
    # heapq.heappush(frontier, (0, start))

    # while frontier:
    #     _, (r, c) = heapq.heappop(frontier)
    #     new_cost = 0 if r == 0 and c == 0 else (grid[r][c] + min(cost[x][y] for x, y in neighbours(r, c)))
    #     # print(frontier)
    #     # pdb.set_trace()
    #     if new_cost < cost[r][c]:
    #         cost[r][c] = new_cost
    #         for x, y in neighbours(r, c):
    #             heapq.heappush(frontier, (new_cost, (x, y)))
    #         # frontier.update(neighbours(r, c))

    # return cost[num_rows - 1][num_cols - 1]
    # def improves(node, frontier):
    #     return all(
    #         node.score < m.score
    #         for m in frontier
    #         if node.row == m.row and node.col == m.col
    #     )

    # for it in count(0):
    #     # print("{:5d} {}".format(it, len(frontier))
    #     if not frontier:
    #         assert False
    #     # print(frontier, end=" → ")
    #     s, n = heapq.heappop(frontier)
    #     # print(node)
    #     # print(node.path_str())
    #     # print(node.score)
    #     # print()
    #     # pdb.set_trace()
    #     if is_goal(node):
    #         return s
    #     explored.add(n)
    #     for n in neighbours(*n):
    #         score_neigh =
    #         if m not in explored and (r, c) not in frontier:
    #             heapq.heappush(frontier, add_score(m, s))
    #         elif (r, c) in frontier and score[r][c] < score[r][c]:
    #             update child


def dijkstra(grid):
    import heapq

    start = (0, 0)
    end = max(grid.keys())

    cost = {pos: np.inf for pos in grid.keys()}
    cost[start] = 0

    frontier = {node: np.inf for node in grid.keys()}
    frontier[start] = 0

    while frontier:
        curr_node, curr_cost = min(frontier.items(), key=lambda t: t[1])
        del frontier[curr_node]

        if curr_node is end:
            return curr_cost

        r, c = curr_node
        for neigh in [(r - 1, c), (r, c - 1), (r, c + 1), (r + 1, c)]:
            new_cost = cost[curr_node] + grid[curr_node]
            if neigh in cost and cost[neigh] > new_cost:
                cost[neigh] = new_cost
                if neigh in frontier:
                    frontier[neigh] = new_cost


def day16(inp):
    from dataclasses import dataclass

    @dataclass
    class Packet:
        version: int

    @dataclass
    class Literal(Packet):
        value: int

    @dataclass
    class Operator(Packet):
        type_id: int
        packets: List[Packet]

    def parse(inp):
        def read_next(n, inp):
            return inp[:n], inp[n:]

        def read_next_int(n, inp):
            val, rest = read_next(n, inp)
            return int(val, 2), rest

        version, inp = read_next_int(3, inp)
        type_id, inp = read_next_int(3, inp)

        if type_id == 4:
            groups = []
            while True:
                group, inp = read_next(5, inp)
                groups.append(group[1:])
                if group.startswith("0"):
                    break
            value = int(cat(groups), 2)
            packet = Literal(version, value)
        else:
            length_type, inp = read_next(1, inp)
            if length_type == "0":
                total_length, inp = read_next(15, inp)
                total_length = int(total_length, 2)
                inp, rest = read_next(total_length, inp)
                packets = []
                while inp:
                    packet, inp = parse(inp)
                    packets.append(packet)
                inp = rest
            else:
                num_sub_packets, inp = read_next(11, inp)
                num_sub_packets = int(num_sub_packets, 2)
                packets = []
                for _ in range(num_sub_packets):
                    packet, inp = parse(inp)
                    packets.append(packet)
            packet = Operator(version, type_id, packets)
        return packet, inp

    def sum_attr(attr, packet):
        if isinstance(packet, Literal):
            return getattr(packet, attr)
        elif isinstance(packet, Operator):
            return getattr(packet, attr) + sum(
                sum_attr(attr, p) for p in packet.packets
            )

    def prod(xs):
        return reduce(lambda x, acc: x * acc, xs, 1)

    def eval_packet(packet: Packet) -> int:
        if isinstance(packet, Literal):
            return packet.value
        elif isinstance(packet, Operator) and packet.type_id == 0:
            return sum(eval_packet(p) for p in packet.packets)
        elif isinstance(packet, Operator) and packet.type_id == 1:
            return prod(eval_packet(p) for p in packet.packets)
        elif isinstance(packet, Operator) and packet.type_id == 2:
            return min(eval_packet(p) for p in packet.packets)
        elif isinstance(packet, Operator) and packet.type_id == 3:
            return max(eval_packet(p) for p in packet.packets)
        elif isinstance(packet, Operator) and packet.type_id == 5:
            m, n = [eval_packet(p) for p in packet.packets]
            return int(m > n)
        elif isinstance(packet, Operator) and packet.type_id == 6:
            m, n = [eval_packet(p) for p in packet.packets]
            return int(m < n)
        elif isinstance(packet, Operator) and packet.type_id == 7:
            m, n = [eval_packet(p) for p in packet.packets]
            return int(m == n)

    packet, _ = parse(inp)
    return sum_attr("version", packet), eval_packet(packet)


def day17a(x_lim, y_lim):
    def gen_traj_x(v):
        x = 0
        while True:
            yield x
            x = x + v
            v = max(0, v - 1)
            # if x > x_lim[1]:
            #     break
            if v == 0:
                yield x
                break

    def gen_traj_y(v):
        y = 0
        while True:
            yield y
            y = y + v
            v = v - 1
            if y < y_lim[0]:
                break

    def extend_traj_x(xs):
        return chain(xs[:-1], repeat(xs[-1]))

    def is_valid_x(xs):
        return any(x_lim[0] <= x <= x_lim[1] for x in xs)

    def is_valid_y(ys):
        return any(y_lim[0] <= y <= y_lim[1] for y in ys)

    def is_valid_xy(xys):
        return any(
            x_lim[0] <= x <= x_lim[1] and y_lim[0] <= y <= y_lim[1] for x, y in xys
        )

    traj_x = [list(gen_traj_x(v)) for v in range(1, x_lim[1] + 1)]
    traj_y = [list(gen_traj_y(v)) for v in range(1, abs(y_lim[0]) + 1)]

    traj_xy = [list(zip(extend_traj_x(x), y)) for x, y in list(product(traj_x, traj_y))]
    traj_xy_valid = [xys for xys in traj_xy if is_valid_xy(xys)]

    # print(traj_xy)
    # print(traj_xy_valid)
    # pdb.set_trace()
    return max(y for xys in traj_xy_valid for _, y in xys)


def day17b(x_lim, y_lim):
    def gen_traj_x(v):
        x = 0
        while True:
            yield x
            x = x + v
            v = max(0, v - 1)
            # if x > x_lim[1]:
            #     break
            if v == 0:
                yield x
                break

    def gen_traj_y(v):
        y = 0
        while True:
            yield y
            y = y + v
            v = v - 1
            if y < y_lim[0]:
                break

    def extend_traj_x(xs):
        return chain(xs[:-1], repeat(xs[-1]))

    def is_valid_x(xs):
        return any(x_lim[0] <= x <= x_lim[1] for x in xs)

    def is_valid_y(ys):
        return any(y_lim[0] <= y <= y_lim[1] for y in ys)

    def is_valid_xy(xys):
        return any(
            x_lim[0] <= x <= x_lim[1] and y_lim[0] <= y <= y_lim[1] for x, y in xys
        )

    traj_x = [list(gen_traj_x(v)) for v in range(1, x_lim[1] + 1)]
    traj_y = [list(gen_traj_y(v)) for v in range(y_lim[0], abs(y_lim[0]) + 1)]

    traj_xy = [list(zip(extend_traj_x(x), y)) for x, y in list(product(traj_x, traj_y))]
    traj_xy_valid = [xys for xys in traj_xy if is_valid_xy(xys)]

    # print(traj_xy)
    # print(traj_xy_valid)
    # pdb.set_trace()
    return len(traj_xy_valid)


def day20step(algorithm, data):
    grid, limits, outside = data
    x_min, x_max, y_min, y_max = limits

    def neighbours(pos):
        x, y = pos
        steps = [-1, 0, 1]
        return [(x + dx, y + dy) for dx in steps for dy in steps]

    def iterate_pos_new():
        return product(range(x_min - 1, x_max + 1), range(y_min - 1, y_max + 1))

    def get_val_at(pos):
        x, y = pos
        if x_min <= x < x_max and y_min <= y < y_max:
            return int(pos in grid)
        else:
            return outside

    def compute_val(pos):
        bin_seq = (get_val_at(n) for n in neighbours(pos))
        index = int(cat(map(str, bin_seq)), 2)
        return algorithm[index]

    grid_new = {}
    for pos in iterate_pos_new():
        if pos not in grid_new:
            grid_new[pos] = compute_val(pos)

    grid_new = {pos for pos, val in grid_new.items() if val}
    limits_new = x_min - 1, x_max + 1, y_min - 1, y_max + 1
    outside_new = algorithm[int(cat(map(str, 9 * [outside])), 2)]

    return grid_new, limits_new, outside_new


def day21a(pos):
    def clock(x):
        v = x % 10
        return 10 if v == 0 else v

    score = [0, 0]
    player = cycle(range(2))
    die = cycle(range(1, 101))
    for num_throws in count(1):
        i = next(player)
        throws = list(take(3, die))
        pos[i] = clock(pos[i] + sum(throws))
        score[i] = score[i] + pos[i]
        print(i, throws, pos, score)
        # pdb.set_trace()
        if score[i] >= 1000:
            other = int(not i)
            return 3 * num_throws * score[other]


def day21b(positions):
    Game = namedtuple("Game", "positions, scores, num_throws, player")
    DIE = range(1, 4)
    MAX_SCORE = 21

    def clock(x):
        v = x % 10
        return 10 if v == 0 else v

    def is_finished(game):
        return game.scores[0] >= MAX_SCORE or game.scores[1] >= MAX_SCORE

    def winner(game):
        return 0 if game.scores[0] >= MAX_SCORE else 1

    def step(game) -> Iterable[Game]:
        if is_finished(game):
            yield game
        else:
            for throws in product(DIE, DIE, DIE):
                if game.player == 0:
                    player = 1
                    positions = (
                        clock(game.positions[0] + sum(throws)),
                        game.positions[1],
                    )
                    scores = (game.scores[0] + positions[0], game.scores[1])
                else:
                    player = 0
                    positions = (
                        game.positions[0],
                        clock(game.positions[1] + sum(throws)),
                    )
                    scores = (game.scores[0], game.scores[1] + positions[1])
                yield Game(positions, scores, game.num_throws + 3, player)

    def compress(elems_with_counts):
        counts_dict = defaultdict(int)
        for elem, count in elems_with_counts:
            counts_dict[elem] += count
        return counts_dict.items()

    games_counts = [(Game(tuple(positions), (0, 0), 0, 0), 1)]
    while not all(is_finished(game) for game, _ in games_counts):
        games_counts = compress(
            concat(zip(step(game), repeat(count)) for game, count in games_counts)
        )
    return compress((winner(game), count) for game, count in games_counts)


def day22a(data):
    def contains(box_out, box_in):
        return (
            box_out[0] <= box_in[0]
            and box_in[1] <= box_out[1]
            and box_out[2] <= box_in[2]
            and box_in[3] <= box_out[3]
            and box_out[4] <= box_in[4]
            and box_in[5] <= box_out[5]
        )

    def agg_switch(acc, curr):
        # t t → t
        # t f → f
        # f t → t
        # f f → f
        if acc and curr:
            return True
        elif acc and not curr:
            return False
        elif not acc and curr:
            return True
        else:
            return False

    # print(data)

    xs = sorted(set(concat(datum[0:2] for _, datum in data)))
    ys = sorted(set(concat(datum[2:4] for _, datum in data)))
    zs = sorted(set(concat(datum[4:6] for _, datum in data)))

    num_on = 0

    for xx in sliding_window(2, xs):
        for yy in sliding_window(2, ys):
            for zz in sliding_window(2, zs):
                box = xx + yy + zz
                switches = [s for s, datum in data if contains(datum, box)]
                switch = reduce(agg_switch, switches, False)
                if switch:
                    num_on += (xx[1] - xx[0]) * (yy[1] - yy[0]) * (zz[1] - zz[0])
                    # print(box)
                    # print(switches)
                    # print(num_on)
                    # print()
                    # pdb.set_trace()
    return num_on


def day22b(data):
    xs = sorted(set(concat(datum[0:2] for _, datum in data)))
    ys = sorted(set(concat(datum[2:4] for _, datum in data)))
    zs = sorted(set(concat(datum[4:6] for _, datum in data)))

    nx = len(xs)
    ny = len(ys)
    nz = len(zs)

    grid = np.zeros((nx - 1, ny - 1, nz - 1), dtype=np.uint8)
    size_x = np.array(xs)[1:] - np.array(xs)[:-1]
    size_y = np.array(ys)[1:] - np.array(ys)[:-1]
    size_z = np.array(zs)[1:] - np.array(zs)[:-1]
    sizes = (
        size_x.reshape(-1, 1, 1) * size_y.reshape(1, -1, 1) * size_z.reshape(1, 1, -1)
    )

    for s, datum in data:
        x1, x2, y1, y2, z1, z2 = datum
        grid[
            xs.index(x1) : xs.index(x2),
            ys.index(y1) : ys.index(y2),
            zs.index(z1) : zs.index(z2),
        ] = s

    return sizes[np.where(grid)].sum()


if current_day == 1:
    data: List[int] = load_data(current_day, int)
    print(day01a(data))
    print(day01b(data))
elif current_day == 2:
    # could have used lifted each entry to monoid and them aggregate them with mconcat

    def parse(line):
        command, step = line.split()
        return command, int(step)

    data: List[tuple] = load_data(current_day, parse)
    print(day02a(data))
    print(day02b(data))
elif current_day == 3:
    # to check a cleaner implementation

    def parse(line):
        return tuple(int(v) for v in line)

    data: List[tuple] = load_data(current_day, parse)
    print(day03a(data))
    print(day03b(data))
elif current_day == 4:
    data: List[str] = load_data(current_day)
    line, *rest = data
    numbers: List[int] = list(map(int, line.split(",")))
    boards: List[List[int]] = list(map(parse_board, partition(6, rest)))
    print(day04a(numbers, boards))
    print(day04b(numbers, boards))
elif current_day == 5:

    def parse_point(p):
        x, y = p.split(",")
        return int(x), int(y)

    def parse(line):
        p1, p2 = line.split(" -> ")
        return parse_point(p1), parse_point(p2)

    data: List[tuple] = load_data(current_day, parse)
    print(day05a(data))
    print(day05b(data))
elif current_day == 6:
    parse = lambda line: list(map(int, line.split(",")))
    numbers: List[int] = load_data(current_day, parse)[0]
    print(day06a(numbers))
    print(day06b(numbers))
elif current_day == 7:
    parse = lambda line: list(map(int, line.split(",")))
    numbers: List[int] = load_data(current_day, parse)[0]
    print(day07a(numbers))
    print(day07b(numbers))
elif current_day == 8:

    def parse(line):
        signal_patterns, outputs = line.split("|")
        return signal_patterns.split(), outputs.split()

    data = load_data(current_day, parse)
    print(day08a(data))
    print(day08b(data))
elif current_day == 9:

    def parse(line):
        return [int(v) for v in line]

    data = load_data(current_day, parse)
    print(day09a(data))
    print(day09b(data))
elif current_day == 10:
    data = load_data(current_day)
    print(day10a(data))
    print(day10b(data))
elif current_day == 11:

    def parse(line):
        return [int(v) for v in line]

    data = load_data(current_day, parse)
    print(day11a(data))
    print(day11b(data))
elif current_day == 12:

    def parse(line):
        x, y = line.split("-")
        return [(x, y), (y, x)]

    edges = multimap(concat(load_data(current_day, parse)))
    print(day12a(edges))
    print(day12b(edges))
elif current_day == 13:

    def parse_points(lines):
        points = []
        for i, line in enumerate(lines):
            if not line:
                break
            else:
                x, y = line.split(",")
                points.append((int(x), int(y)))
        return set(points)

    def parse_fold(lines):
        *_, params = lines.split()
        direction, value = params.split("=")
        return direction, int(value)

    lines1, lines2 = load_data(current_day, sep="\n\n")
    points, lines = parse_points(lines)
    folds = [parse_fold(line) for line in lines]
    print(day13(points, folds, "a"))
    day13(points, folds, "b")
elif current_day == 14:

    def parse_rule(line):
        p, γ = line.split(" -> ")
        α, ω = p
        return (p, (α + γ)), (p, (γ + ω))

    line_polymer, lines_rules = load_data(current_day, sep="\n\n")

    polymer = dict(Counter(map(cat, sliding_window(2, line_polymer))))
    rules = multimap(concat(parse_rule(line) for line in lines_rules.split("\n")))

    print(day14(polymer, rules, num_steps=10))
    print(day14(polymer, rules, num_steps=40))
elif current_day == 15:

    def printg(grid):
        for row in grid:
            for elem in row:
                print(elem, end=" ")
            print()
        print()

    def add_mod_10(v, x):
        x = x + v
        return x % 10 + 1 if x >= 10 else x

    def mapg(f, grid):
        return [[f(elem) for elem in row] for row in grid]

    def transpose(grid):
        return list(map(list, zip(*grid)))

    def hcat(grid1, grid2):
        return [row1 + row2 for row1, row2 in zip(grid1, grid2)]

    def vcat(grid1, grid2):
        return transpose(hcat(transpose(grid1), transpose(grid2)))

    def hcatv(*grids):
        return reduce(hcat, grids)

    def vcatv(*grids):
        return reduce(vcat, grids)

    def tile(tl, tr, bl, br):
        return vcat(hcat(tl, tr), hcat(bl, br))

    def parse(line):
        return [int(v) for v in line]

    def enlarge(base_grid, curr_grid, step):
        if step == 1:
            return grid
        else:
            side = [
                mapg(lambda x: add_mod_10(i, x - 1), curr_grid)
                for i in range(step, 2 * step - 1)
            ]
            tl = enlarge(base_grid, curr_grid, step - 1)
            br = mapg(lambda x: add_mod_10(2 * step - 2, x), curr_grid)
            return tile(tl, vcatv(*side), hcatv(*side), br)

    grid = load_data(15, parse)
    grid = enlarge(grid, grid, 5)
    num_rows = len(grid)
    num_cols = len(grid[0])

    def neighbours(r, c):
        coords = [
            (r - 1, c),
            (r, c - 1),
            (r, c + 1),
            (r + 1, c),
        ]
        return [(x, y) for x, y in coords if 0 <= x < num_rows and 0 <= y < num_cols]

    import networkx as nx

    G = nx.DiGraph()
    for r, row in enumerate(grid):
        for c, elem in enumerate(row):
            for x, y in neighbours(r, c):
                G.add_edge((r, c), (x, y), weight=grid[x][y])
    cost = nx.shortest_path_length(
        G, (0, 0), (num_rows - 1, num_cols - 1), weight="weight"
    )
    print(cost)

    # print(day15a(grid))
    # print(day15b(grid))
    # sys.setrecursionlimit(150_000)
    # printg(grid_lg)
    # pdb.set_trace()
    # print(day15a(grid))
    # grid1 = {(r, c): elem for r, row in enumerate(grid) for c, elem in enumerate(row)}
    # print(dijkstra(grid1))
    # print(path)
    # for u, v in sliding_window(2, path):
    #     print(G.edges[u, v])
    # pdb.set_trace()
    # import random
    # for i in range(10):
    #     grid = mapg(lambda _: random.choice(range(1, 10)), grid)
    #     grid_lg = enlarge(grid, grid, 5)
    #     assert day15a(grid_lg) == day15b(grid_lg)
    # I was not able to implement a fast enough version of Dijkstra's algorithm, so I have resorted existing implementations :-(
elif current_day == 16:
    lines = load_data(0)

    def hex_to_bin(n):
        return "{:04b}".format(int(n, 16))

    for line in lines:
        inp = cat(hex_to_bin(c) for c in line)
        print(day16(inp))
elif current_day == 17:
    # target area: x=257..286, y=-101..-57
    x_lim = (20, 30)
    y_lim = (-10, -5)
    # target area: x=257..286, y=-101..-57
    x_lim = [257, 286]
    y_lim = [-101, -57]
    print(day17a(x_lim, y_lim))
    print(day17b(x_lim, y_lim))
elif current_day == 20:

    def print_grid(data):
        grid, limits, _ = data
        x_min, x_max, y_min, y_max = limits
        for r in range(x_min, x_max):
            for c in range(y_min, y_max):
                char = "#" if (r, c) in grid else "."
                print(char, end="")
            print()
        print()

    to_num = lambda s: 1 if s == "#" else 0
    lines1, lines2 = load_data(current_day, sep="\n\n")
    lines2 = lines2.split()

    algorithm = list(map(to_num, lines1))

    grid0 = {
        (r, c)
        for r, row in enumerate(lines2)
        for c, elem in enumerate(row)
        if elem == "#"
    }
    limits0 = 0, len(lines2), 0, len(lines2[0])
    outside0 = 0
    data0 = grid0, limits0, outside0

    step = partial(day20step, algorithm)

    data = nth(2, iterate(step, data0))
    print(len(data[0]))

    data = nth(50, iterate(step, data0))
    print(len(data[0]))
elif current_day == 21:
    # pos = [4, 8] # test example
    pos = [10, 7]  # my input
    print(day21a(pos))
    print(day21b(pos))
elif current_day == 22:

    def clamp(x):
        return max(-50, min(x, 51))

    def parse(line, to_clamp=True):
        switch = line.startswith("on")
        vals = ints(line)
        x1, x2, y1, y2, z1, z2 = vals
        vals = x1, x2 + 1, y1, y2 + 1, z1, z2 + 1
        if to_clamp:
            vals = tuple(map(clamp, vals))
        return switch, vals

    data = load_data(current_day, parse)
    print(day22a(data))
    print(day22b(data))
    data = load_data(current_day, lambda line: parse(line, False))
    print(day22b(data))
